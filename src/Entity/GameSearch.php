<?php

namespace App\Entity;

class GameSearch
{
    /**
     * @var string|null
     */
    private $gameChoice;

    /**
     * @var string|null
     */
    private $dayChoice;

    /**
     * @return string|null
     */
    public function getDayChoice(): ?string
    {
        return $this->dayChoice;
    }

    /**
     * @param string $dayChoice
     * @return GameSearch
     */
    public function setDayChoice(string $dayChoice): GameSearch
    {
        $this->dayChoice = $dayChoice;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGameChoice(): ?string
    {
        return $this->gameChoice;
    }

    /**
     * @param string $gameChoice
     * @return GameSearch
     */
    public function setGameChoice(string $gameChoice): GameSearch
    {
        $this->gameChoice = $gameChoice;
        return $this;
    }
}