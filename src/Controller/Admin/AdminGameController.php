<?php
namespace App\Controller\Admin;

use App\Entity\Game;
use App\Form\GameType;
use App\Repository\GameRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminGameController extends AbstractController
{
    /**
     * @var GameRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(GameRepository $repository, ObjectManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @return Response
     * @Route("/admin", name="admin.game.index")
     */
    public function index()
    {
        $games = $this->repository->findAll();
        return $this->render('admin/game/index.html.twig', compact('games'));
    }

    /**
     * @Route("admin/game/create", name="admin.game.new")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function new(Request $request)
    {
        $game = new Game();
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($game);
            $this->em->flush();
            $this->addFlash('success', 'Créer avec succès');
            return $this->redirectToRoute('admin.game.index');
        }

        return $this->render('admin/game/new.html.twig', [
            'game' => $game,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/{id}", name="admin.game.edit")
     * @param Game $game
     * @param Request $request
     * @return Response
     */
    public function edit(Game $game, Request $request)
    {
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', 'Modifier avec succès');
            return $this->redirectToRoute('admin.game.index');
        }

        return $this->render('admin/game/edit.html.twig', [
            'game' => $game,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/property/{id}", name="admin.game.delete", methods="DELETE")
     * @param Game $game
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Game $game, Request $request)
    {
        if($this->isCsrfTokenValid('delete' . $game->getId(), $request->get('_token'))) {
            $this->em->remove($game);
            $this->em->flush();
            $this->addFlash('success', 'Supprimer avec succès');
        }
        return $this->redirectToRoute('admin.game.index');
    }
}