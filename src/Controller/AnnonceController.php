<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\GameSearch;
use App\Form\GameSearchType;
use App\Repository\GameRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class AnnonceController extends AbstractController
{

    /**
     * @var GameRepository
     */
    private $repository;

    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(GameRepository $repository, ObjectManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/games", name="game.index")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $search = new GameSearch();
        $form =$this->createForm(GameSearchType::class, $search);
        $form->handleRequest($request);

        $games = $paginator->paginate(
            $this->repository->findAllVisibleQuery($search),
            $request->query->getInt('page', 1),
            12
        );
        return $this->render('annonces/index.html.twig', [
            'games' => $games,
            'current_menu' => 'games',
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/games/{id}", name="game.show")
     * @param Game $game
     * @return Response
     */
    public function show(Game $game) : Response
    {
        return $this->render('annonces/show.html.twig', [
            'game' => $game,
            'current_menu' => 'games'
        ]);
    }

}