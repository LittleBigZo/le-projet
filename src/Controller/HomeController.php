<?php

namespace App\Controller;

use App\Repository\GameRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @return Response
     * @param GameRepository $repository
     */
    public function index(GameRepository $repository): Response
    {
        $games = $repository->findLatest();
        return $this->render('pages/home.html.twig', [
            'games' => $games
        ]);
    }
}