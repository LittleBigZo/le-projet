<?php

namespace App\Repository;

use App\Entity\Game;
use App\Entity\GameSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
    }

    /**
     * @return Query
     * @throws \Exception
     */
    public function findAllVisibleQuery(GameSearch $search) : Query
    {
        $query = $this->findActiveQuery();

        if($search->getDayChoice())
        {
            $query = $query
                ->andWhere('g.date = :date')
                ->setParameter('date', new \DateTIME('now'));
        }

        if($search->getGameChoice())
        {
            $query = $query
                ->andWhere('g.videoGameId = :jeu')
                ->setParameter('jeu', $search->getGameChoice());
        }

        return $query->getQuery();
    }

    /**
     * @return Game[]
     */
    public function findLatest() : array
    {
        return $this->findActiveQuery()
            ->setMaxResults(8)
            ->getQuery()
            ->getResult();
    }

    private function findActiveQuery() : QueryBuilder
    {
        return $this->createQueryBuilder('g')
            ->where('g.date > :date')
            ->orderBy('g.date', 'asc')
            ->setParameter('date', new \DateTIME('now'));
    }

    // /**
    //  * @return Game[] Returns an array of Game objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Game
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
