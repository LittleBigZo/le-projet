<?php

namespace App\DataFixtures;

use App\Entity\Game;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class GameFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for($i = 0; $i <100; $i++)
        {
            $game = new Game();
            $game->setVideoGameId($faker->words(3, true))
                ->setCreator($faker->numberBetween(1, 100))
                ->setDate($faker->dateTime($max = '2030-12-12', $timezone = null))
                ->setCreatedAt($faker->dateTime($max = '2030-12-12', $timezone = null));
            $manager->persist($game);
        }

        $manager->flush();
    }
}
